#!/usr/bin/env python3
from __future__ import print_function, unicode_literals

import os
import io
import re
import sys
import time
import argparse
import tarfile
import shutil
from io import StringIO
from datetime import datetime
from os.path import dirname, abspath, join, basename, isfile

from prettytable import PrettyTable

import logging
LOG_FORMAT = '%(levelname)-10s %(asctime)s %(name)s %(pathname)s #%(lineno)d: %(message)s'
logging.basicConfig(level='DEBUG', format=LOG_FORMAT)
log = logging.getLogger(__name__)

HERE = abspath(dirname(__file__))

# use timestamp, then we don't override old data
# PREFIX = datetime.now().strftime('%Y%m%d%H%M%S')

MODEL = join(HERE, "{{model_file_name}}")
DNS_QUERY_FILE = join(HERE, "{{ samba_dns_query_file }}")

# data dir for this run
STATS_DIR_NAME = "{{stats_dir_name}}"
STATS_DIR = join(HERE, STATS_DIR_NAME)

LATENCY = 5.0
DURATION = 300


CMD = """
/usr/bin/time -v -p -a -o {output} script/traffic_replay -U {{samba_username}}%{{samba_password}}  \
--realm {{samba_realm}} \
--workgroup {{samba_domain|upper}} \
--fixed-password iegh1haevoofoo3looT9  \
--latency-timeout {LATENCY} \
-r {r} \
-T {T} \
--dns-rate {dns_rate} \
--dns-query-file dns-query.txt \
--random-seed=1 \
--preserve-tempdir \
-D {DURATION} \
--debuglevel 0 \
{MODEL}  {{primary_dc_name}}.{{samba_realm}} \
> {output}
"""


def get_output_path(r, S, dns_rate=0):
    return join(STATS_DIR, 'r{:02}-T{:02}-dns{}.txt'.format(r, S, int(dns_rate)))


def replay(r, T, dns_rate=0):
    output = get_output_path(r, T, dns_rate=dns_rate)
    if os.path.exists(output):
        with io.open(output, mode='rt', encoding='utf8') as text_file:
            text = text_file.read()
        if 'Total conversations' in text:
            log.info('skip {}'.format(output))
            return
        else:
            os.remove(output)
    cmd = CMD.format(r=r, T=T, dns_rate=int(dns_rate),
                     MODEL=MODEL,
                     DNS_QUERY_FILE=DNS_QUERY_FILE,
                     output=output,
                     DURATION=DURATION,
                     LATENCY=LATENCY)
    time.sleep(1)
    log.info(cmd)
    ret = os.system(cmd)
    if ret != 0:
        log.error('exit with cmd out put: {}'.format(ret))
        exit(ret)

# stat line example:
# cldap             3  searchRequest                                  1            0     0.240764     0.240764     0.240764     0.000000     0.240764
STAT_LINE_PATTERN = r"""^
(\w+)\s+       # Protocol
(\d+)\s+       # Op Code
\w+\s+         # Description
\d+\s+         # Count
\d+\s+         # Failed
\d+\.\d+\s+    # Mean
\d+\.\d+\s+    # Median
(\d+\.\d+)\s+  # 95%
\d+\.\d+\s+    # Range
\d+\.\d+$      # Max
"""

STAT_LINE_FLAGS = re.MULTILINE | re.VERBOSE

STAT_LINE_RE = re.compile(STAT_LINE_PATTERN, flags=STAT_LINE_FLAGS)


class StatFileParser:

    def __init__(self, path):
        assert isfile(path)
        self.path = path
        self.text = io.open(path).read()
        self.name = basename(path)
        m = re.match(r'^r(\d+)-T(\d+)-dns(\d+).txt', self.name)
        self.is_name_valid = False
        self.error = 0
        self.number = 0.0
        self.number_display = ''
        if m:
            self.is_name_valid = True
            r, T, dns = m.group(1, 2, 3)  # strs
            self.r = int(r)
            self.T = int(T)
            self.dns = int(dns)
            self.key = (self.r, self.T, self.dns)

            try:
                self.parse()
            except Exception:
                log.exception('fail to parse %s', self.name)
                self.error = 1
                self.number_display = 'X'  # X means error

    def parse(self):
        pattern = r'Successful operations:\s+\d+\s+\((\d+\.\d+) per second\)'
        m = re.search(pattern, self.text)
        self.successful_operations_per_second = float(m.group(1))

        #  pattern = r'Unfinished conversations:\s+(\d+)'
        #  m = re.search(pattern, self.text)
        #  self.unfinished_conversations = int(m.group(1))

        if self.is_latency_acceptable():
            self.number = self.successful_operations_per_second
            self.number_display = '{:.3f}'.format(self.number)
        else:
            self.number_display = 'L'  # L means high latency

    def is_latency_acceptable(self):
        """
        Find 95% column, ignore file data if latency too large

        it's really important that binds are fast for developer testing
        you can uncomment/add below to ignore a particular bad result
        """
        latency_map = {
            # TODO: ignore ldap bind for now
            # ('ldap', 0): 1.0,  # ldap bind
            # ('ldap', 3): 999.9,  # ldap search
        }
        for i in range(100):
            latency_map[('dns', i)] = 0.5

        lines = STAT_LINE_RE.findall(self.text)
        for protocol, opcode, number in lines:
            key = (protocol.strip(), int(opcode))
            latency = latency_map.get(key, LATENCY)
            number = float(number)
            if number >= latency:
                log.warn('%s latency too high %s: %.3f > %.3f', self.name, key, number, latency)
                return False
        return True


class DefaultTrafficMode:
    errors = 0
    best_number = 0.0
    best_param = (None, None, None)  # r, T, dns_rate
    dns_rate = 0
    # r, T_RANGE
    # We go in smaller steps in the central range.
    # Outside ranges may be useful if performance gets radically better or worse.
    # Current failure point is T=~190.
    r = set(range(80, 320, 8))
    r.update(range(160, 260, 4))
    r.update(range(190, 240, 2))
    TABLE = [
        (1, sorted(r)),
    ]

    def replay(self):
        for r, T_range in self.TABLE:
            for T in T_range:
                replay(r, T, dns_rate=self.dns_rate)

    def get_stats(self, root=STATS_DIR):
        stats = {}
        for filename in os.listdir(root):
            filepath = os.path.join(root, filename)
            obj = StatFileParser(filepath)
            key = getattr(obj, 'key', None)
            if key:
                stats[obj.key] = obj
                self.errors += obj.error
                if obj.error == 0:
                    number = obj.number
                    if number and type(number) == float:
                        if self.best_number < number:
                            self.best_number = number
                            self.best_param = obj.key
        return stats

    def get_table(self, stats):
        """
        T\r    1      2
        100    180.2  190.2
        200    185.2  199.7
        300    185.2  199.7
        """
        r_set = set()
        T_set = set()
        for r, T_range in self.TABLE:
            r_set.add(r)
            for T in T_range:
                T_set.add(T)
        r_list = sorted(r_set)
        T_list = sorted(T_set)

        t = PrettyTable(align='r')
        t.field_names = [r"T\r"] + r_list
        for T in T_list:
            row = [T]
            for r in r_list:
                key = (r, T, self.dns_rate)
                obj = stats.get(key)
                row.append(obj.number_display if obj else '')
            t.add_row(row)
        return t

    def summary(self):

        stats = self.get_stats()
        table = self.get_table(stats)

        text = ''
        text += '\nbest_number: {}'.format(self.best_number)
        text += '\nbest_param(r, T, dns_rate): {}'.format(self.best_param)
        text += '\nerrors: {}'.format(self.errors)
        text += '\nX: error'
        text += '\nL: high latency'
        text += '\n'
        text += str(table)
        text += '\n'
        print(text)

        # write summary to file
        txtname = os.path.join(STATS_DIR, 'summary.txt')
        with open(txtname, 'w') as txt:
            txt.write(text)

        # make a tarfile for stats dir
        tarname = '{}.tgz'.format(STATS_DIR)
        with tarfile.open(tarname, 'w:gz') as tar:
            tar.add(STATS_DIR, arcname=STATS_DIR_NAME)


class DNSTrafficMode(DefaultTrafficMode):
    # in dns mode, we fix r
    r = 1
    # T and dns_rate_range
    TABLE = [
        (150, list(range(100, 2001, 100))),
    ]

    def replay(self):
        for T, dns_rate_range in self.TABLE:
            for dns_rate in dns_rate_range:
                replay(self.r, T, dns_rate=dns_rate)

    def get_table(self, stats):
        """
        dns\T  160    170
        100    180.2  190.2
        200    185.2  199.7
        """
        T_set = set()
        dns_rate_set = set()
        for T, dns_rate_range in self.TABLE:
            T_set.add(T)
            for dns_rate in dns_rate_range:
                dns_rate_set.add(dns_rate)
        T_list = sorted(T_set)
        dns_rate_list = sorted(dns_rate_set)

        t = PrettyTable(align='r')
        t.field_names = [r"dns\T"] + T_list

        for dns_rate in dns_rate_list:
            row = [dns_rate]
            for T in T_list:
                key = (self.r, T, dns_rate)
                obj = stats.get(key)
                row.append(obj.number_display if obj else '')
            t.add_row(row)
        return t


def get_traffic_mode(mode):
    return DNSTrafficMode() if mode == "dns" else DefaultTrafficMode()


def main():

    parser = argparse.ArgumentParser(
        description='Run traffic replay against DC')

    parser.add_argument(
        '-r', '--replay', action='store_true',
        help='run traffic replay')

    parser.add_argument(
        '--mode', choices=['default', 'dns'], default='{{ samba_traffic_mode }}',
        help='run traffic replay in which mode')

    parser.add_argument(
        '-c', '--cleanup', action='store_true',
        help='clean up stats dir')

    parser.add_argument(
        '-s', '--summary', action='store_true',
        help='get traffic summary')

    parser.add_argument(
        '-a', '--all', action='store_true',
        help='run replay and get summary')

    args = parser.parse_args()

    has_task = False

    if args.cleanup:
        log.info('removing stats dir...')
        shutil.rmtree(STATS_DIR, ignore_errors=True)
        has_task = True
    os.system('mkdir -p {}'.format(STATS_DIR))

    log.info('running in %s mode', args.mode)
    mode = get_traffic_mode(args.mode)

    if args.all or args.replay:
        has_task = True
        mode.replay()
    if args.all or args.summary:
        mode.summary()
        has_task = True

    if not has_task:
        parser.print_help()


if __name__ == '__main__':
    main()
